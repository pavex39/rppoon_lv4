using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset csvFile = new Dataset(@"C:\Users\Petar Pavic\Desktop\csv.csv");
            Adapter adapter = new Adapter(new Analyzer3rdParty());
            double[][] matrix = adapter.ConvertData(csvFile);
            double[] averages = adapter.CalculateAveragePerColumn(csvFile);
            for(int i = 0; i < averages.Length; i++)
            {
                Console.WriteLine(averages[i]);
            }

            Adapter.ToString(matrix);
        }
    }
}
